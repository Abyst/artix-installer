# Artix Installer

A simple installer for Artix Linux.

## Usage

DO NOT USE. WORK IN PROGRESS.

### Connect to internet

#### WPA

	rfkill unblock wlan
	wpa_supplicant -B -i <INTERFACE> \
		-c <(wpa_passphrase <SSID> <PASSPHRASE>)
	dhclient <INTERFACE>

You can see your interface with `ip addr`.

See more at the [Arch Wiki](https://wiki.archlinux.org/title/wpa_supplicant).

### Download dependencies

```sh
# pacman -Sy git jq
```

### Run installer

	git clone https://gitlab.com/Abyst/artix-installer.git
	./artix-installer/artix-installer

You will be prompted to generate a configuration.

## Important information

This installer is based on [Zaechus' Artix Installer](https://github.com/Zaechus/artix-installer).

