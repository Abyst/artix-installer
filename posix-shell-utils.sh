# Posix Shell Utils
# 
# Source: https://gitlab.com/Abyst/posix-shell-utils-sh
# Built on: 2024-01-29 22:59:21 UTC
# 
# Copyright (c) 2024 Abyst
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the
# Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall
# be included in all copies or substantial portions of the
# Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Source: function.sh
# Modified: 2024-01-25 01:36:01 UTC

func() {
	local -
	subcommand="$1"
	shift
	
	case "$subcommand" in
	help)
		schema="$(cat)"
		
		subcommands="$(printf "%s" "$schema" \
			| jq .subcommands)"
		if [ "$subcommands" != null ]; then
			printf "Subcommands:\n"
			keys="$(printf "%s" "$subcommands" \
				| jq -r '. | keys_unsorted[]')"
			
			width="$(for key in $keys; do
				printf "%s\n" "$key"
			done | wc -L)"
			
			for key in $keys; do
				inspected_key="$(printf "%s" "$key" \
					| jq -Rs '.')"
				value="$(printf "%s" "$subcommands" \
					| jq -r ".$inspected_key")"
				
				description="$(printf "%s" "$value" \
					| jq -r '.description')"
				
				printf "  %-*s  %s\n" \
					"$width" "$key" "$description"
			done
			printf "\n"
		fi
		
		flags="$(printf "%s" "$schema" \
			| jq '.flags')"
		if [ "$flags" != null ]; then
			printf "Flags:\n"
			keys="$(printf "%s" "$flags" \
				| jq -r '. | keys_unsorted[]')"
			
			width="$(for key in $keys; do
				inspected_key="$(printf "%s" "$key" \
					| jq -Rs '.')"
				value="$(printf "%s" "$flags" \
					| jq -r ".$inspected_key")"
	
				tiny="-X,"
				long="--$key"
				type="$(printf "%s" "$value" \
					| jq '.type')"
				if [ "$type" != null ]; then
					type=" <$(printf "%s" "$type" \
						| jq -r '.')>"
				else
					type=""
				fi
				
				printf "%s %s%s\n" "$tiny" "$long" "$type"
			done | wc -L)"
			
			for key in $keys; do
				inspected_key="$(printf "%s" "$key" \
					| jq -Rs '.')"
				value="$(printf "%s" "$flags" \
					| jq -r ".$inspected_key")"
				
				tiny="$(printf "%s" "$value" \
					| jq .tiny)"
				if [ "$tiny" != null ]; then
					tiny="-$(printf "%s" "$tiny" \
						| jq -r '.'),"
				else
					tiny="   "
				fi
				long="--$key"
				type="$(printf "%s" "$value" \
					| jq .type)"
				if [ "$type" != null ]; then
					type=" <$(printf "%s" "$type" \
						| jq -r .)>"
				else
					type=""
				fi
				left="$tiny $long$type"
				description="$(printf "%s" "$value" \
					| jq -r '.description')"
				printf "  %-*s  %s\n" \
					"$width" "$left" "$description"
			done
			printf "\n"
		fi
		
		parameters="$(printf "%s" "$schema" \
			| jq '.parameters')"
		if [ "$parameters" != null ]; then
			printf "Parameters:\n"
			keys="$(printf "%s" "$parameters" \
				| jq -r '. | keys_unsorted[]')"
			
			width="$(for key in $keys; do
				inspected_key="$(printf "%s" "$key" \
					| jq -Rs '.')"
				value="$(printf "%s" "$parameters" \
					| jq -r ".$inspected_key")"
				
				type="$(printf "%s" "$value" \
					| jq '.type')"
				if [ "$type" != null ]; then
					type="<$(printf "%s" "$type" \
						| jq -r .)>"
				else
					type=""
				fi
				case "$(printf "%s" "$value" \
					| jq '.optional')" in
				true)
					optional="?"
					;;
				false | null)
					optional=""
					;;
				esac
				
				printf "%s%s %s\n" "$key" "$optional" "$type"
			done | wc -L)"
			
			for key in $keys; do
				inspected_key="$(printf "%s" "$key" \
					| jq -Rs '.')"
				value="$(printf "%s" "$parameters" \
					| jq -r ".$inspected_key")"
				
				type="$(printf "%s" "$value" \
					| jq '.type')"
				if [ "$type" != null ]; then
					type="<$(printf "%s" "$type" \
						| jq -r '.')>"
				else
					type=""
				fi
				case "$(printf "%s" "$value" \
					| jq '.optional')" in
				true)
					optional="?"
					;;
				false | null)
					optional=""
					;;
				esac
				
				left="$key$optional $type"
				description="$(printf "%s" "$value" | jq \
					-r .description)"
				printf "  %-*s  %s\n" \
					"$width" "$left" "$description"
			done
			printf "\n"
		fi
		;;
	parse)
		schema="$(cat)"
		
		# Subcommands
		if [ $# -gt 0 ]; then
			keys="$(printf "%s" "$schema" \
				| jq -r '.subcommands // {} | keys_unsorted[]')"
			arg="$1"
			for key in $keys; do
				if [ "$arg" = "$key" ]; then
					jq -n '$ARGS.named' \
						--arg subcommand "$key" \
						| Value
					return
				fi
			done
		fi
		
		parsed="{}"
		
		# Flags
		keys="$(printf "%s" "$schema" \
			| jq -r '.flags // {} | keys_unsorted[]')"
		while [ $# -gt 0 ]; do
			arg="$1"
			if [ "$arg" = "--" ]; then
				shift
				break
			fi
			
			flag=null
			for key in $keys; do
				inspected_key="$(printf "%s" "$key" \
					| jq -Rs '.')"
				flag="$(printf "%s" "$schema" \
					| jq ".flags.$inspected_key")"
				
				long="$key"
				if [ "$arg" = "--$long" ]; then
					break
				fi
				
				tiny="$(printf "%s" "$flag" \
					| jq '.tiny')"
				if [ "$tiny" != null ]; then
					tiny="$(printf "%s" "$tiny" \
						| jq -r '.')"
					if [ "$arg" = "-$tiny" ]; then
						break
					fi
				fi
				
				flag=null
			done
			
			if [ "$flag" = null ]; then
				break
			fi
			shift
			
			type="$(printf "%s" "$flag" \
				| jq '.type')"
			if [ "$type" = null ]; then
				parsed="$(printf "%s" "$parsed" \
					| jq '.flags += $ARGS.named' \
						--argjson "$key" true)"
			else
				type="$(printf "%s" "$type" \
					| jq -r .)"
				value="$(printf "%s" "$1" \
					| type parse "$type")"
				error="$(printf "%s" "$value" \
					| jq '.error')"
				if [ "$error" != null ]; then
					printf "%s" "$error" \
						| jq '{error: [$left, .] | join(" ")}' \
							--arg left "Flag ($key):"
					return
				fi
				value="$(printf "%s" "$value" \
					| jq '.value')"
				
				shift
				parsed="$(printf "%s" "$parsed" \
					| jq '.flags += $ARGS.named' \
						--argjson "$key" "$value")"
			fi
		done
		
		# Parameters
		parameters="$(printf "%s" "$schema" \
			| jq -r '.parameters // {} | keys_unsorted[]')"
		while [ $# -gt 0 ]; do
			key="$(printf "%s" "$parameters" \
				| head -n1)"
			if [ "$key" = "" ]; then
				printf "%s" "Too many parameters" \
					| jq -Rs '{error: .}'
				return
			fi
			
			inspected_key="$(printf "%s" "$key" \
				| jq -Rs '.')"
			parameter="$(printf "%s" "$schema" \
				| jq ".parameters.$inspected_key")"
			
			parameters="$(printf "%s" "$parameters" \
				| sed '1d')"
			
			arg="$1"
			shift
			
			type="$(printf "%s" "$parameter" \
				| jq '.type')"
			if [ "$type" = null ]; then
				printf "%s" "Parameter without a type" \
					| jq -Rs '{error: .}'
				return
			fi
			
			value="$arg"
			type="$(printf "%s" "$type" \
				| jq -r .)"
			value="$(printf "%s" "$value" \
				| type parse "$type")"
			error="$(printf "%s" "$value" \
				| jq '.error')"
			if [ "$error" != null ]; then
				printf "%s" "$error" \
					| jq '{error: [$left, .] | join(" ")}' \
						--arg left "Parameter ($key):"
				return
			fi
			value="$(printf "%s" "$value" \
				| jq '.value')"
			
			parsed="$(printf "%s" "$parsed" \
				| jq '.parameters += $ARGS.named' \
					--argjson "$key" "$value")"
		done
		
		if [ "$(printf "%s" "$parameters")" != "" ]; then
			printf "%s" "Not enough parameters" \
				| jq -Rs '{error: .}'
		else
			Value "$parsed"
		fi
		;;
	*)
		Error "Invalid subcommand"
		;;
	esac
}

# Source: input.sh
# Modified: 2024-01-29 22:52:33 UTC

input_list() {
	local -
	schema="$(cat <<-EOF
	{
	  "subcommands": {
	    "help": {
	      "description": "Show help"
	    }
	  },
	  "flags": {
	    "multiple": {
	      "tiny": "m",
	      "description": "Use multiple results"
	    }
	  },
	  "parameters": {
	     "options": {
	       "description": "Options array",
	       "type": "List"
	     },
	     "prompt": {
	       "description": "Prompt for the user",
	       "type": "String",
	       "optional": true
	     }
	  }
	}
	EOF
	)"
	parsed="$(printf "%s" "$schema" \
		| func parse "$@")"
	error="$(printf "%s" "$parsed" \
		| jq .error)"
	if [ "$error" != null ]; then
		printf "%s" "$schema" \
			| func help \
			| jq -Rs '$ARGS.named + {help: .}' \
				--argjson error "$error"
		return
	fi
	parsed="$(printf "%s" "$parsed" \
		| jq '.value')"
	
	subcommand="$(printf "%s" "$parsed" \
		| jq '.subcommand')"
	if [ "$subcommand" != null ]; then
		shift
		case "$(printf "%s" "$subcommand" \
			| jq -r '.')" in
		help)
			printf "%s" "$schema" \
				| func help \
				| jq -Rs '{help: .}'
			;;
		esac
	else
		multiple="$(printf "%s" "$parsed" \
			| jq -r '.flags."multiple" // false')"
		prompt="$(printf "%s" "$parsed" \
			| jq -r '.parameters."prompt"')"
		
		options="$(printf "%s" "$parsed" \
			| jq '.parameters."options"')"
		n_options="$(printf "%s\n" "$options" \
			| jq 'length')"
		selected="$(printf "%s" "$options" \
			| jq 'map(false)')"
		pretty_options="$(printf "%s" "$options" \
			| jq -c '.[]' \
			| while IFS= read -r option; do
				printf "%s\n" "$option" \
					| jq '.' \
					| sed ':a;N;$!ba;s/,\n\s\{0,\}/, /g' \
					| sed ':a;N;$!ba;s/\n\s\{0,\}//g'
			done \
			| head -c -1 \
			| jq -Rs 'split("\n")')"
		>&2 tput smcup
		height=$(($(tput lines) - 2))
		n_pages=$(($n_options / $height + 1))
		
		cursor=0
		page=0
		while true; do
			>&2 tput clear
			>&2 printf "%s" "$prompt"
			if [ $n_pages -gt 1 ]; then
				>&2 printf " [page %u/%u]" \
					$(($page + 1)) \
					$n_pages
			fi
			begin=$(($page * $height))
			end=$(($begin + $height))
			if [ $end -gt $(($n_options - 1)) ]; then
				end=$(($n_options - 1))
			fi
			
			list="$(for i in $(seq $begin $end); do
				if [ $i -eq $cursor ]; then
					printf "\n> "
				else
					printf "\n  "
				fi
				if [ "$multiple" = true ]; then
					is_selected="$(printf "%s" "$selected" \
						| jq ".[$i]")"
					case "$is_selected" in
					true)
						printf "[x] "
						;;
					false)
						printf "[ ] "
						;;
					esac
				fi
				option="$(printf "%s" "$pretty_options" \
					| jq -r ".[$i]")"
				printf "%s" "$option"
			done)"
			>&2 printf "%s" "$list"
			
			stty raw -echo
			raw="$(dd bs=8 count=1 2> /dev/null)"
			stty -raw echo
			
			code="$(printf "%s" "$raw" \
				| hexdump \
				| head -n1 \
				| xargs \
				| cut -c9-)"
			
			case "$code" in
			'5b1b 0041') # Up
				if [ $cursor -eq $begin ]; then
					cursor=$end
				else
					cursor=$(($cursor - 1))
				fi
				;;
			'5b1b 0042') # Down
				if [ $cursor -eq $end ]; then
					cursor=$begin
				else
					cursor=$(($cursor + 1))
				fi
				# cursor=$((($cursor + 1) % $n_options))
				;;
			'5b1b 0043') # Right
				page=$((($page + 1) % $n_pages))
				cursor=$(($page * $height))
				;;
			'5b1b 0044') # Left
				if [ $page -eq 0 ]; then
					page=$(($n_pages - 1))
				else
					page=$(($page - 1))
				fi
				cursor=$(($page * $height))
				;;
			'0003') # Ctrl-C
				output="$(printf "%s" "Interrupt" \
					| jq -Rs '{error: .}')"
				break
				;;
			'0020') # Space
				if [ "$multiple" = true ]; then
					selected="$(printf "%s" "$selected" \
						| jq ".[$cursor] = (.[$cursor] | not)")"
				fi
				;;
			'000d') # Enter
				case "$multiple" in
				true)
					output="$(jq -n '$options
						| to_entries
						| map(select($selected[.key]).value)
						| {value: .}' \
						--argjson options "$options" \
						--argjson selected "$selected")"
					;;
				false)
					output="$(printf "%s" "$options" \
						| jq "{value: .[$cursor]}")"
					;;
				esac
				break
				;;
			esac
		done
		>&2 tput rmcup
		
		printf "%s" "$output"
	fi
}

input() {
	local -
	schema="$(cat <<-EOF
	{
	  "subcommands": {
	    "list": {
	      "description": "Interactive list selection"
	    },
	    "help": {
	      "description": "Show help"
	    }
	  },
	  "flags": {
	    "num-chars": {
	      "tiny": "n",
	      "type": "Int",
	      "description": "Number of characters to read"
	    },
        "supress-output": {
	      "tiny": "s",
	      "description": "Don't print keystroke values"
	    }
	  },
	  "parameters": {
	     "prompt": {
	       "description": "Prompt for the user",
	       "type": "String",
	       "optional": true
	     }
	  }
	}
	EOF
	)"
	
	parsed="$(printf "%s" "$schema" \
		| func parse "$@")"
	error="$(printf "%s" "$parsed" \
		| jq .error)"
	if [ "$error" != null ]; then
		printf "%s" "$schema" \
			| func help \
			| jq -Rs '$ARGS.named + {help: .}' \
				--argjson error "$error"
		return
	fi
	parsed="$(printf "%s" "$parsed" \
		| jq '.value')"
	
	subcommand="$(printf "%s" "$parsed" \
		| jq '.subcommand')"
	if [ "$subcommand" != null ]; then
		shift
		case "$(printf "%s" "$subcommand" \
			| jq -r '.')" in
		list)
			input_list "$@"
			;;
		help)
			printf "%s" "$schema" \
				| func help \
				| jq -Rs '{help: .}'
			;;
		esac
	else
		prompt="$(printf "%s" "$parsed" \
			| jq -r '.parameters."prompt"')"
		num_chars="$(printf "%s" "$parsed" \
			| jq -r '.flags."num-chars"')"
		supress_output="$(printf "%s" "$parsed" \
			| jq -r '.flags."supress-output" // false')"
		
		>&2 printf "%s" "$prompt"
		if [ "$supress_output" = true ]; then
			stty -echo
		fi
		if [ "$num_chars" != null ]; then
			stty raw
			string="$(dd bs=8 count="$num_chars" 2> /dev/null)"
			stty -raw
		else
			read -r string
		fi
		if [ "$supress_output" = true ]; then
			stty echo
			>&2 printf "\n"
		fi
		
		printf "%s" "$string" \
			| jq -Rs '{value: .}'
	fi
}

# Source: panic.sh
# Modified: 2024-01-25 01:36:11 UTC

panic() {
	local -
	red="$(tput setaf 1)"
	reset="$(tput sgr0)"
	
	>&2 printf "%s%s%s\n" "$red" "$1" "$reset"
	shift
	while [ $# -gt 0 ]; do
		inspected="$(printf "%s" "$1" | jq -Rs .)"
		>&2 printf "%s\n" "$red" "$inspected" "$reset"
		shift
	done
	exit 1
}

# Source: regex.sh
# Modified: 2024-01-25 01:36:22 UTC

match() {
	local -
newline="
"
	if [ $# -ne 1 ]; then
		>&2 cat <<-EOF
			$0:$LINENO: Expected 1 argument, got $#
		EOF
		return 1
	fi
	regex="^$1\$"
	
	n_matches="$(printf "%s" "$regex" | rg '[^\\]\(' \
		--count-matches)"
	[ $? -eq 0 ] || return 1
	
	replacement=""
	for i in $(seq 1 $n_matches); do
		replacement="$replacement\$$i$newline"
	done
	
	rg "$regex" -r "$replacement"
	[ $? -eq 0 ] || return 1
}

capture() {
	local -
	if [ $# -ne 1 ]; then
		>&2 cat <<-EOF
			$0:$LINENO: Expected 1 argument, got $#
		EOF
		return 1
	fi
	sed "${1}q;d"
}

# Source: result.sh
# Modified: 2024-01-25 02:42:34 UTC

Value() {
	case $# in
	0)
		jq '{value: .}'
		;;
	1)
		printf "%s" "$1" \
			| jq '{value: .}'
		;;
	esac
}

Error() {
	case $# in
	0)
		jq -Rs '{error: .}'
		;;
	1)
		printf "%s" "$1" \
			| jq -Rs '{error: .}'
		;;
	esac
}

Main() {
	local -
	result="$(cat)"
	
	error="$(printf "%s" "$result" \
		| jq '.error')"
	if [ "$error" != null ]; then
		printf "%s" "$error" \
			| >&2 jq -r '.'
	fi
	
	help="$(printf "%s" "$result" \
		| jq '.help')"
	if [ "$help" != null ]; then
		printf "%s" "$help" \
			| jq -r '.' \
			| >&2 bat -p -lhelp
	fi
	
	value="$(printf "%s" "$result" \
		| jq '.value')"
	if [ "$value" != null ]; then
		printf "%s" "$value" \
			| jq -r '.'
	fi
	
	if [ "$error" != null ]; then
		return 1
	else
		return 0
	fi
}

Unwrap() {
	local -
	result="$(cat)"
	
	error="$(printf "%s" "$result" \
		| jq '.error')"
	if [ "$error" != null ]; then
		printf "%s" "$error" \
			| >&2 jq -r '.'
	fi
	
	help="$(printf "%s" "$result" \
		| jq '.help')"
	if [ "$help" != null ]; then
		printf "%s" "$help" \
			| jq -r '.' \
			| >&2 bat -p -lhelp
	fi
	
	if [ "$error" != null ]; then
		return 1
	fi
}

# Source: type.sh
# Modified: 2024-01-24 16:43:44 UTC

type() {
	local -
	case "$1" in
	parse)
		shift
		input="$(cat)"
		type="$1"
		case "$type" in
		'Int')
			printf "%s" "$input" \
				| rg --quiet '^\d+$'
			if [ $? -ne 0 ]; then
				jq -n '$ARGS.named' \
					--arg error "Not an Int"
			else
				jq -n '$ARGS.named' \
					--argjson value "$input"
			fi
			;;
		'String')
			jq -n '$ARGS.named' \
				--arg value "$input"
			;;
		'List')
			type="$(printf "%s" "$input" \
				| jq -r '. | type')"
			if [ "$type" != 'array' ]; then
				jq -n '$ARGS.named' \
					--arg error "Not a List"
			else
				jq -n '$ARGS.named' \
					--argjson value "$input"
			fi
			;;
		*)
			type="$(printf "%s" "$type" \
				| jq -Rs '.')"
			jq -n '$ARGS.named' \
				--arg error "Invalid type $type"
			;;
		esac
	esac
}

